Before running the program please run the script to create the database or if you have a database of your own
change the database connection properties in the application.properties file as the configurations are for my local Dev database.

CREATE DATABASE assessment
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
    
The generated billing file (txt) is saved in the billing_files directory and each file has got a suffix of the year and month the file was created to enable track keeping of the files.
    
ND - The file upload api method or functionality is not implemented.

For the billing question or rather Scenario 1 run the AutoBillingApplication.java
For the streams question or rather Scenario 2 run the StreamsDemo.java