package com.absa.billing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.absa.billing.service.BillingServiceImp;

@SpringBootApplication
public class AutoBillingApplication {


	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(AutoBillingApplication.class, args);
		BillingServiceImp billingService = context.getBean(BillingServiceImp.class);		
	}
}
