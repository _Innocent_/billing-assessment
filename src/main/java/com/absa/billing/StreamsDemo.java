package com.absa.billing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsDemo {
    public static void main(String[] args) {
        List<String> statuses = Arrays.asList("Received", "Pending", "", "Awaiting Maturity", "Completed", "");
        List<String> nonEmptyStatuses = streamFilterNonEmptyStatuses(statuses);

        for (String status: nonEmptyStatuses) {
            System.out.println(status);
        }
    }

    //Using Streams
    private static List<String> streamFilterNonEmptyStatuses(List<String> statuses){
        return  statuses.stream().filter(str -> !str.isEmpty()).collect(Collectors.toList());
    }

    //Not Using Streams
    private static List<String> filterNonEmptyStatuses(List<String> statuses){

        List<String> filteredStatuses = new ArrayList<>();

        for (String status: statuses) {
            if(!status.isEmpty()){
                filteredStatuses.add(status);
            }
        }

        return  filteredStatuses;
    }
}
