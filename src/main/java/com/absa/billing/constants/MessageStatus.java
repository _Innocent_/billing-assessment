package com.absa.billing.constants;

public enum MessageStatus {
	Completed, 
	CurrencyRejected
}
