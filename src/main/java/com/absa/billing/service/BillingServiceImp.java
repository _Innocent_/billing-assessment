package com.absa.billing.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.absa.billing.constants.Currency;
import com.absa.billing.entity.Bill;
import com.absa.billing.resource.BillingRepository;
import com.absa.billing.service.baseService.BinningService;

@Component
@EnableScheduling
public class BillingServiceImp implements BinningService {

	@Autowired
	BillingRepository repository;

	public BillingServiceImp() {
	}

	@PostConstruct
	private void init() {
		System.out.println("Service Created");
		saveBilling();
	}

	/**
	 * Scheduling a task to be executed at 07:00 AM of every day of every month from
	 * Monday to Friday. second, minute, hour, day of month, month, day(s) of week
	 * scheduling.billing= 0 0 7 1-3 * MON-FRI
	 */
	@Override
	@Scheduled(cron = "0 0 7 1-3 * MON-FRI")
	public void creatBilling() {
		if (isFirstBusinessDay()) {
			List<Bill> bills = getPreviousMonthsTransations();
			generateBillingFile(bills);
			uploadFile();
		}
	}

	/*
	 * Method to retrieve transactions for the previous month that has status
	 * Completed
	 */
	public List<Bill> getPreviousMonthsTransations() {

		Date perviousMonthStartDate = getPreviousMonthStartDate();
		Date perviousMonthEndDate = getPreviousMonthEndDate();
		List<Bill> bills = repository.findAllWithCreationDateTimeBefore(perviousMonthStartDate, perviousMonthEndDate);

		return bills;
	}

	private Date getPreviousMonthEndDate() {

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		calendar.set(Calendar.HOUR, 23);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		calendar.set(Calendar.MILLISECOND, 00);

		return calendar.getTime();
	}

	private Date getPreviousMonthStartDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		calendar.set(Calendar.MILLISECOND, 00);
		return calendar.getTime();
	}

	private String getFileName() {

		SimpleDateFormat fileNameDateFormatter = new SimpleDateFormat("YYYY_MM");
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		String filePath = "billing_files/";
		return filePath + "Billing_" + fileNameDateFormatter.format(calendar.getTime()) + ".txt";
	}

	private boolean fileExists() {
		File file = new File(getFileName());
		return file.exists();
	}

	/*
	 * Method to generate the billing file for the previous month's billing only
	 * records that has status of Completed which are passed as a parameter
	 */
	public void generateBillingFile(List<Bill> bills) {

		BufferedWriter bw = null;
		try {
			/*
			 * To successfully keep track or record of the billing files each file has
			 * suffix of the previous month and year in format MM_YYYY
			 */
			String fileName = getFileName();

			File file = new File(fileName);
			FileOutputStream fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));

			/*
			 * 
			 */
			String serviceName = "INTEGRATEDSERVICES";
			Map<String, List<Bill>> mapBills = new HashMap<String, List<Bill>>();

			for (Bill bill : bills) {
				String key = bill.getClientSwiftAddress();

				if (!mapBills.containsKey(key)) {
					mapBills.put(key, new ArrayList<Bill>());
				}

				mapBills.get(key).add(bill);
			}

			SimpleDateFormat fileNameDateFormatter = new SimpleDateFormat("YYYYMMDD");
			Calendar calendar = Calendar.getInstance();

			for (String key : mapBills.keySet()) {
				Bill bill = mapBills.get(key).get(0);
				int usageStatus = mapBills.get(key).size();

				bw.write(serviceName + key + bill.getCurrency() + fileNameDateFormatter.format(calendar.getTime())
						+ usageStatus);
				bw.newLine();
			}

		} catch (FileNotFoundException e) {
			// File was not found
			e.printStackTrace();
		} catch (IOException e) {
			// Problem when writing to the file
			e.printStackTrace();
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/*
	 * This method uploads bills or data to the table i just call in in the init
	 * method of this service because i didn't want to create a database script
	 */
	public void saveBilling() {

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, -1);

		List<Bill> bills = new ArrayList<>();

		bills.add(new Bill("transactionRefer", "HSBCXXXYYCC", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYCC", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYPP", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYCC", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYCC", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYCC", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYCC", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYBB", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYCC", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYBB", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYPP", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYBB", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYBB", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYPP", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYPP", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYPP", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYBB", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYBB", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYPP", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYPP", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYPP", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYBB", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYBB", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYBB", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYPP", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYBB", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYYY", "Completed", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));
		bills.add(new Bill("transactionRefer", "HSBCXXXYYXX", "Rejected", Currency.ZAROUT.toString(), 1000,
				calendar.getTime()));

		for (Bill bill : bills) {
			repository.save(bill);
		}
	}

	private boolean isFirstBusinessDay() {
		if (!fileExists()) {
			return true;
		}
		return false;
	}

	/*
	 * This should be the implementation of the function to upload the generated
	 * billing file to the shared location eg.. sharepoint
	 */
	private void uploadFile() {
		// To be implemented
	}

}
