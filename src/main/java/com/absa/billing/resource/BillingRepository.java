package com.absa.billing.resource;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.absa.billing.entity.Bill;

public interface BillingRepository extends JpaRepository<Bill, Long> {
	
	@Query("select b from billing b where b.dateTimeCreated >= :startDate and b.dateTimeCreated <= :endDate")
    List<Bill> findAllWithCreationDateTimeBefore(
      @Param("startDate") Date startDate, @Param("endDate") Date endDate);
}