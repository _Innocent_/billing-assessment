package com.absa.billing.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.absa.billing.entity.baseEntity.CoreEntity;

@Entity(name = "billing")
public class Bill extends CoreEntity{
	
	@Column(length = 16)
	private String transactionReference;
	
	@Column(length = 12)
	private String clientSwiftAddress;
	
	@Column(length = 9)
	private String messageStatus;
	
	@Column(length = 6)
	private String currency;
	
	private double amount;
	
	@Column(length = 6)
	private Date dateTimeCreated;

	public Bill() {
		super();
	}

	public String getTransactionReference() {
		return transactionReference;
	}
	
	public Bill(String transactionReference, String clientSwiftAddress, String messageStatus, String currency,
			double amount, Date dateTimeCreated) {
		super();
		this.transactionReference = transactionReference;
		this.clientSwiftAddress = clientSwiftAddress;
		this.messageStatus = messageStatus;
		this.currency = currency;
		this.amount = amount;
		this.dateTimeCreated = dateTimeCreated;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	public String getClientSwiftAddress() {
		return clientSwiftAddress;
	}

	public void setClientSwiftAddress(String clientSwiftAddress) {
		this.clientSwiftAddress = clientSwiftAddress;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDateTimeCreated() {
		return dateTimeCreated;
	}

	public void setDateTimeCreated(Date dateTimeCreated) {
		this.dateTimeCreated = dateTimeCreated;
	}
}
